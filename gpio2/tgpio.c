#include "tgpio.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/mman.h>

static TBANK banks[] = {
	{'A', 0x00, 0x10},
	{'B', 0x04, 0x14},
	{'C', 0x08, 0x18},
	{'D', 0x0c, 0x1c},
	{'E', 0x20, 0x24},
	{'F', 0x30, 0x34},
	{'G', 0x38, 0x3c},
	{'H', 0x40, 0x44}
};

/* GPIO memory mapped registers */
volatile unsigned int *PEDR;
volatile unsigned int *PEDDR;

//9 egpio <-> egpio 11
//B1            B3

TBANK *portdata(char port)
{
	int i;
	TBANK *rs=NULL;
	for(i=0; i < (sizeof(banks)/sizeof(*banks)); i++) {
		if(banks[i].port == port) {
			rs = &banks[i];
			break;
		}
	}
	return rs;
}

int read_port(char port, int bit)
{
	int rs = -1;
	TBANK *bank = portdata(port);
	if(bank) {
#ifdef DEBUG
		printf("Reading...\n");
#endif
		unsigned int mask = 1 << bit;
		unsigned int neg_mask = ~(1 << bit);
		int fd = open("/dev/mem", O_RDWR);
		if (fd < 0) {
			perror("Failed to open /dev/mem");
			exit(fd);
		}
		unsigned char *gpio = mmap(0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_BASE);
    
		PEDR = (unsigned int *)(gpio + bank->port_offset);
		PEDDR = (unsigned int *)(gpio + bank->dir_offset);

#ifdef DEBUG
		printf("Dir before = 0x%0x\n", *PEDDR);
#endif
		*PEDDR &= neg_mask;//set bit for input
#ifdef DEBUG
		printf("Dir after = 0x%0x\n", *PEDDR);
#endif
		rs = (*PEDR) & mask;
#ifdef DEBUG
		printf("Port read = 0x%0x\n", *PEDR);
#endif
		close(fd);
		rs = rs != 0;
	}
	return rs;
}

void write_port(char port, int bit, int val)
{
	TBANK *bank = portdata(port);
	if(bank) {
#ifdef DEBUG
		printf("Writing...\n");
#endif
		unsigned int mask = 1 << bit;
		unsigned int neg_mask = ~(1 << bit);
    
		int fd = open("/dev/mem", O_RDWR);
		if (fd < 0) {
			perror("Failed to open /dev/mem");
			exit(fd);
		}
		unsigned char *gpio = mmap(0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_BASE);
    
		PEDR = (unsigned int *)(gpio + bank->port_offset); //port B
		PEDDR = (unsigned int *)(gpio + bank->dir_offset);
    
#ifdef DEBUG
		printf("Dir before = 0x%0x\n", *PEDDR);
#endif
		*PEDDR |= mask;//set bit for output
#ifdef DEBUG
		printf("Dir after = 0x%0x\n", *PEDDR);
		printf("Port before = 0x%0x\n", *PEDR);
#endif
		unsigned int port = (val) ? (*PEDR | mask) : (*PEDR & neg_mask);
		*PEDR = port;
#ifdef DEBUG
		printf("Port written = 0x%0x -> 0x%0x\n", port, *PEDR);
#endif
		close(fd);
	} else {
		printf("Wrong port %s\n", port);
	}
}


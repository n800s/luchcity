#ifndef __TGPIO_H

#define __TGPIO_H

#define GPIO_BASE 0x80840000

typedef struct _TBANK {
	const char port;
	unsigned char port_offset;
	unsigned char dir_offset;
} TBANK;

void write_port(char port, int bit, int val);
int read_port(char port, int bit);

#endif //__TGPIO_H

#include "tgpio.h"
#include <stdio.h>
#include <unistd.h>

int main (int argc, char **argv)
{
	int ch;
	char mode = 0;
	int quiet = 0;
	char port = 0;
	int pin = -1;
	int val = -1;
	int rs;
	while((ch = getopt(argc, argv, "m:p:b:v:q")) != -1)
    {
        switch(ch)
        {
            case 'm': mode = optarg[0]; break;
            case 'p': port = optarg[0]; break;
            case 'b': pin = atoi(optarg); break;
            case 'v': val = atoi(optarg); break;
            case 'q': quiet = 1; break;
        }
    }
	if((mode != 'w' && mode !='r') || port == 0 || pin < 0 || (val < 0 && mode == 'w')) {
		printf("Usage: %s -m <w|r> -p <port> -b <pin> [-v <1|0>]\n", argv[0]);
	} else {
		switch(mode) {
			case 'w':
				write_port(port, pin, val);
				if(!quiet) {
					printf("Written %d to port %c, pin %d\n", val, port, pin);
				}
				break;
			case 'r':
				rs = read_port(port, pin);
				if(!quiet) {
					printf("Reading port %c, pin %d = %d\n", port, pin, rs);
				} else {
					printf("%d\n", rs);
				}
				break;
		}
	}
	return 0;
}

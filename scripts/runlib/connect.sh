#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/../data/init.inc
. $SCRIPTPATH/lib.sh

if ps -Af | grep -v grep | grep pppd
then
	lecho "Running pppd found. Use it"
else
	lecho "Start new pppd"
#	pppd call MTS-modem
fi

n=0
while ! ping -q -c 1 ${PING_HOST}
do
	n=`expr $n + 1`
	lecho "Can not ping $PING_HOST time $n so far"
	sleep ${PING_WAIT}
done
exit 0

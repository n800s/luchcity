#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/../data/init.inc
. $SCRIPTPATH/lib.sh

lecho 'Loading schedule...'
max_attempts=10
re1=$max_attempts
while [ $re1 -ne 0 ]; do
	lecho "Removing old $SCHNAME.tmp if it exists"
	rm -f $SCHNAME.tmp 2>/dev/null
	lecho "Loading schedule time `expr $max_attempts - $re1 + 1`"
	if [ -n "$TION_PRO2" ]; then
		#for tion2 no https available
		cmd="wget '$SCHEDULER_URL?$MAC' -O '$SCHNAME.tmp'"
	else
		cmd="wget --no-check-certificate '$SCHEDULER_URL?$MAC' -O '$SCHNAME.tmp'"
	fi
	err=`eval $cmd 2>&1`
	if [ -s $SCHNAME.tmp ]; then
		#test integrity
		crc=`sed -n '1p' $SCHNAME.tmp`
		lecho "CRC from loaded file: $crc"
		tot=`$SCRIPTPATH/calc_crc.sh $SCHNAME.tmp`
		lecho "CRC calculated: $tot"
		if [ "$crc" = "$tot" ]
		then
			err=`cp $SCHNAME.tmp $SCHNAME 2>&1`
			if [ $? -eq 0 ]
			then
				lecho "$SCHNAME.tmp copied to $SCHNAME"
				re1=0
				lecho 'Schedule has been loaded'
			else
				lecho "Error copying $SCHNAME.tmp to $SCHNAME: $err"
			fi
		else
			lecho "Error: CRC $crc != $tot"
			re1=`expr $re1 - 1`
		fi
	else
		lecho "`date +%s`: wget failed: $err"
		sleep 3
		re1=`expr $re1 - 1`
	fi
done

#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/../data/init.inc
. $SCRIPTPATH/lib.sh

rs=0
lecho "Switching Off"
if [ -n "$TION_PRO2" ]
then
	lecho "Turning light off for tion pro 2"
	$SCRIPTPATH/../bin/gpiorw -m w -p ${LIGHT_PORT} -b ${LIGHT_PIN} -v 0
	rs=$?
else
	if [ -n "$TION_PRO28" ]
	then
		lecho "Turning light off for tion pro 28"
		echo $LIGHT_GPIO > /sys/class/gpio/export && \
		echo out > "/sys/class/gpio/gpio${LIGHT_GPIO}/direction" && \
		echo 0 > "/sys/class/gpio/gpio${LIGHT_GPIO}/value" && \
		echo $LIGHT_GPIO > /sys/class/gpio/unexport
		rs=$?
	else
		lecho "Neither Tion pro2 nor Tion pro28 so no light switchable"
		rs=0
	fi
fi
if [ $rs -ne 0 ]; then
	lecho 'Error while switching light off'
fi
exit $rs

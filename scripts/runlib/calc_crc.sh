#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/../data/init.inc
. $SCRIPTPATH/lib.sh

rs=0
if [ -z $1 ]
then
	fname=$SCHNAME
else
	fname=$1
fi

lecho "Caclulating CRC for $fname"
#test integrity
unset tot
DONE=false
until $DONE
do
	read -r  line || DONE=true
	lecho "Line '$line' has been read"
	if [ -z $tot ]
	then
		tot=0
	else
		[ -z "$line" ] && continue
		lecho "Using '$line' for CRC calculation"
		d=$(echo "${line}" | cut -d ' ' -f 1)
		for i in 1 2 3
		do
			vm=$(echo "${d}" | cut -d '.' -f $i)
			tot=`expr $tot + $vm`
		done
		for i in 2 3
		do
			v=$(echo "${line}" | cut -d ' ' -f $i)
			for j in 1 2
			do
				vm=$(echo "${v}" | cut -d ':' -f $j)
				tot=`expr $tot + $vm`
			done
		done
	fi
done < $fname
rs=$?
lecho CRC=$tot
echo $tot
exit $rs

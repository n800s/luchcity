# in the end of program it is better to call lecho End
# to output all repeated lines
lecho() {
	if [ $DEBUG_LOG -eq 1 ]
	then
		if [ "x${log_line_count}" = "x" ]
		then
			log_line_count=0
		fi
		if [ "x${last_logline}" != "x$@" ]
		then
			time_prefix="[`date '+%F %T'`]($$) $LOG_PREFIX"
			if [ $log_line_count -gt 1 ]
			then
				echo ... >>/tmp/luchcity.log
				echo "$time_prefix $last_logline ($log_line_count times repeated)" >>$LOG_FILE
			fi
			echo "$time_prefix $@" >>$LOG_FILE
			last_logline=$@
			log_line_count=1
		else
			log_line_count=$(($log_line_count + 1))
		fi
	fi
}

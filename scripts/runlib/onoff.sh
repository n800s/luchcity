#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/../data/init.inc
. $SCRIPTPATH/lib.sh


if ! [ -f $SCHNAME ]
then
	fname=$DEFAULT_SCHNAME
else
	fname=$SCHNAME
fi
lecho "Processing $fname"

#sets $s1 and $s2, uses $line
make_seconds() {
	local dl t1 t2 rs
	if [ $(echo "${line}" | wc -w) -eq 3 ]
	then
		rs=0
		ln1=$(echo "${line}" | cut -d ' ' -f 1)
		dl=""
		for i in 1 2 3
		do
			dl="$dl $(echo "${ln1}"|cut -d '.' -f $i)"
		done
		if [ $(echo "${dl}" | wc -w) -lt 3 ]
		then
			dl="$dl `date +%Y`"
		fi
		t1=$(echo "${line}" | cut -d' ' -f2)
		t2=$(echo "${line}" | cut -d' ' -f3)
		set $dl
		if [ -n "$TION_PRO2" ]
		then
			cmd="date '+%s' -d \"$2$1`echo ${t1}|sed "s/://g"`$3\""
			lecho "Executing $cmd ..."
			s1=`eval $cmd`
			lecho "Result is $s1"
			cmd="date '+%s' -d \"$2$1`echo ${t2}|sed "s/://g"`$3\""
			lecho "Executing $cmd ..."
			s2=`eval $cmd`
			lecho "Result is $s2"
		else
			cmd="date '+%s' -d \"$3-$2-$1 $t1:00\""
			lecho "Executing $cmd ..."
			s1=`eval $cmd`
			lecho "Result is $s1"
			cmd="date '+%s' -d \"$3-$2-$1 $t2:00\""
			lecho "Executing $cmd ..."
			s2=`eval $cmd`
			lecho "Result is $s2"
		fi
		lecho "s1=$s1, s2=$s2"
		if [ $s1 -gt $s2 ]
		then
			lecho "It is over midnight range. Add 24h to s2"
			s2=`expr $s2 + 24 \* 3600`
			lecho "s2 becomes $s2"
		fi
	else
		lecho "'$line' skipped as it does not have 3 elements"
		rs=1
	fi
	return $rs
}

lecho 'Applying scheduler...'
on=0

#make today range
line="`date +%d.%m.%Y` 00:00 23:59"
make_seconds
today_start=$s1
today_end=$s2
lecho "Today range: $today_start $today_end"
yesterday_start=`expr $today_start - 3600 \* 24`
yesterday_end=`expr $today_end - 3600 \* 24`
lecho "Yesterday range: $yesterday_start $yesterday_end"
#find line with line that starts today if any
#and the last line that starts before today
ti=0
DONE=false
until $DONE
do
	read -r  line || DONE=true
	if [ "x$line" != "x" ]
	then
		if make_seconds
		then
			lecho "Processing $line as $s1..$s2"
			if [ "x$line_before" = "x" ]
			then
				line_before=$line
			fi
			if [ $s1 -gt $today_end ]
			then
				#period after today
				lecho "Period after today"
				break
			fi
			if [ $s1 -lt $today_start ]
			then
				line_before=$line
			else
				lecho "Today line: $line: $s1 .. $s2"
				ti=`expr $ti + 1`
				eval "line$ti=\"$line\""
				eval "pb$ti=$s1"
				eval "pe$ti=$s2"
			fi
		fi
	fi
done < $fname

lecho "Last line $line_before"
#make last line yesterday seconds
line="`date +%d.%m.%Y` $(echo "${line_before}" | cut -d ' ' -f2,3)"
make_seconds
line0=yesterday
pb0=`expr $s1 - 3600 \* 24`
pe0=`expr $s2 - 3600 \* 24`
lecho "The last line as yesterday in seconds $pb0 $pe0"
if [ $ti -lt 1 ]
then
	#make last line today seconds
	line="`date +%d.%m.%Y` $(echo "${line_before}" | cut -d ' ' -f2,3)"
	make_seconds
	line0=today
	pb1=$s1
	pe1=$s2
	ti=`expr $ti + 1`
	lecho "The last line as today in seconds $pb1 $pe1"
fi
#now we have pb(begin) and pe(end) seconds each $ti in total

this_secs=`date '+%s'`
lecho "Current time seconds: $this_secs, number of tests: `expr $ti + 1`"
i=0
while [ $i -le $ti ]
do
	eval "s1=\$pb$i"
	eval "s2=\$pe$i"
	eval "line=\"\$line$i\""
	lecho "Testing $i: $s1 .. $this_secs .. $s2 ($line)"
	i=`expr $i + 1`
	if [ $s1 -lt $this_secs ] && [ $s2 -gt $this_secs ]
	then
		lecho 'Found'
		on=1
		break
	fi
done

if [ $on -eq 0 ]
then
	lecho "Light goes off"
	$SCRIPTPATH/light_off.sh
	rs=$?
else
	lecho "Light goes on"
	$SCRIPTPATH/light_on.sh
	rs=$?
fi

exit $rs

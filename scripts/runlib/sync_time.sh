#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/../data/init.inc
. $SCRIPTPATH/lib.sh

lecho 'Synchronizing time...'
lecho "Removing old $TIME_TMPFILE if it exists"
rm -f $TIME_TMPFILE 2>/dev/null
if [ -n "$TION_PRO2" ]; then
	#for tion2 no https available
	cmd="wget '$TIME_URL?$MAC' -O '$TIME_TMPFILE'"
else
	cmd="wget --no-check-certificate '$TIME_URL?$MAC' -O '$TIME_TMPFILE'"
fi
lecho "Running $cmd"
err=`eval $cmd 2>&1`
rs=$?
if [ $rs -eq 0 ]
then
	read dummy1 dummy2 ss <$TIME_TMPFILE
	if [ -z $ss ]
	then
		lecho 'No time received'
		rs=2
	else
		lecho "$ss has been received"
		SS=`date '+%s'`
		if [ "x$ss" = "x$SS" ]
		then
			lecho 'Current time is ok. No adjustment needed'
		else
			if [ -n "$TION_PRO2" ] || [ -n "$TION_PRO28" ]
			then
				date -D %s -s $ss && hwclock -w
				fin=$?
			else
				dt=`date '+%F %T' -d "1970-01-01 $ss sec GMT"` && \
					date -s "$dt" && hwclock -w
				fin=$?
			fi
			if [ $fin -eq 0 ]
			then
				lecho 'Time has been synchronized'
			else
				lecho 'Error synchronizing time'
			fi
		fi
	fi
else
	lecho "Error getting time from $TIME_URL?$MAC: $err"
fi
lecho "now: `date -R`"
exit $rs

#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/../data/init.inc
. $SCRIPTPATH/lib.sh

if ! [ -f $SCHNAME ]
then
	fname=$DEFAULT_SCHNAME
else
	fname=$SCHNAME
fi
lecho "Processing $fname"

#return seconds for $line "%d.%m.%Y %H:%M"
date2seconds() {
	local dl t secs
	ln1=$(echo "${line}" | cut -d ' ' -f 1)
	dl=""
	for i in 1 2 3
	do
		dl="$dl $(echo "${ln1}"|cut -d '.' -f $i)"
	done
	t=$(echo "${line}" | cut -d' ' -f2)
	set $dl
	if [ -n "$TION_PRO2" ]
	then
		cmd="date '+%s' -d \"$2$1`echo ${t}|sed "s/://g"`$3\""
		lecho "Executing $cmd ..."
		secs=`eval $cmd`
		lecho "Result is $secs"
	else
		cmd="date '+%s' -d \"$3-$2-$1 ${t}:00\""
		lecho "T2 Executing $cmd ..."
		secs=`eval $cmd`
		lecho "T2 Result is $secs"
	fi
	echo $secs
}

# input - test_date
# returns schedule line
find_schedule_line() {
	line="$test_date 12:00"
	test_secs=`date2seconds`
	DONE=false
	until $DONE
	do
		read -r  sline || DONE=true
		if [ $(echo "${sline}" | wc -w) -gt 1 ]
		then
			# get date part
			ln1=$(echo "${sline}" | cut -d ' ' -f 1)
			# split date part
			dl=''
			for i in 1 2 3
			do
				dl="$dl $(echo "${ln1}"|cut -d '.' -f $i)"
			done
			# if date part has 2 numbers only then add current year
			if [ $(echo "${dl}" | wc -w) -lt 3 ]
			then
				line="$ln1.`date +%Y` 00:00"
			else
				line="$ln1 00:00"
			fi
			# turn date into seconds
			secs=`date2seconds`
			if [ $secs -gt $test_secs ]
			then
				break
			else
				last_sline=$sline
			fi
		else
			lecho "'$sline' skipped as it does not have 2 or more elements"
		fi
	done < $fname
	# here it prints to return
	if [ -z "$last_sline" ]
	then
		lecho "Error: Current date $test_date is not in schedule"
	fi
	echo $last_sline
}

if [ "x$1" = "x" ]
then
	# today date
	test_date=`date '+%d.%m.%Y'`
	# current time
	test_time=`date '+%H:%M'`
	# work mode
	test_mode=false
else
	test_date=`echo $1|cut -d ' ' -f1`
	test_time=`echo $1|cut -d ' ' -f2`
	# test mode without turning on/off lights
	test_mode=true
fi

schedule_line=`find_schedule_line`
t1=`echo $schedule_line|cut -d ' ' -f2`
t2=`echo $schedule_line|cut -d ' ' -f3`
tt1=`echo $t1|sed 's/://'`
tt2=`echo $t2|sed 's/://'`
ttt=`echo $test_time|sed 's/://'`
if [ $tt1 -gt $tt2 ]
then
	# light period 0-t2 t1-0
	lecho "$t2 < $test_time < $t1 = off else on"
	on=true
	if [ $tt2 -lt $ttt -a $ttt -lt $tt1 ]
	then
		on=false
	fi
else
# light period t1-t2
	lecho "$t1 < $test_time < $t2 = on else off"
	on=false
	if [ $tt1 -lt $ttt -a $ttt -lt $tt2 ]
	then
		on=true
	fi
fi
if $on
then
	if $test_mode
	then
		echo On
		rs=$?
	else
		lecho "Light goes on"
		$SCRIPTPATH/light_on.sh
		rs=$?
	fi
else
	if $test_mode
	then
		echo Off
		rs=$?
	else
		lecho "Light goes off"
		$SCRIPTPATH/light_off.sh
		rs=$?
	fi
fi

exit $rs

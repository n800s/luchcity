#часть от URL
GROUP="luchcity"

#SCHEDULER_URL="http://lights.infohit.ru/$GROUP/lights"
TIME_URL="http://lights.infohit.ru/cgi-bin/time.pl"

#URL расписания
SCHEDULER_URL="http://n800s.dyndns.org/$GROUP/lights"
#URL времени
#TIME_URL="http://n800s.dyndns.org/timestamp"


#SCHEDULER_URL="https://wprof.infohit.ru/aisa/test/get_schedule/2/"
#TIME_URL="https://wprof.infohit.ru/aisa/test/get_timestamp/"

#хост, который пингуется для проверки соединения интернетного
PING_HOST="n800s.ddns.net"

#пауза между неудачными проверками наличия интернета
PING_WAIT=5

#пауза перед проверкой а не пора ли включать свет
LIGHTS_SLEEP=60

#пауза между проверками а не пора ли скачивать расписание и синхронизировать время
CONNECT_SLEEP=5

#пауза перед началом работы включателя света
START_PAUSE=60

#как получить мак адрес, который используется как уникальный идентификатор загрузки расписания
MAC=`/sbin/ifconfig eth0 |grep HWaddr |cut -d H -f 2 | cut -d " " -f 2`

#путь до файла с загруженным расписанием (в первой строке в нем - контрольная сумма)
SCHNAME=/tmp/lights

#путь до файла с загруженным для синхронизации временем
TIME_TMPFILE=/tmp/time.tmp

#файл с расписанием по-умолчанию (в этом файле контрольной суммы нет)
DEFAULT_SCHNAME=$SCRIPTPATH/../data/lights.default

#for tion pro28 для включения света используется пин 53
LIGHT_GPIO=53

#for tion pro2 для включения света используется порт B, пин 1
LIGHT_PORT=B
LIGHT_PIN=1

#если результат не пуская строка, то это тион про2
TION_PRO2=`cat /proc/cpuinfo|grep Hardware|grep 'Tion-Pro2'`

#если результат не пуская строка, то это тион про28
TION_PRO28=`cat /proc/cpuinfo|grep Hardware|grep 'Freescale MX28EVK'`

#если 1 то скрипты выводят кучу всего в лог файл LOG_FILE
#если 0 то не выводят
DEBUG_LOG=1
#файл куда лог писаться будет
#LOG_FILE=/tmp/luchcity.log
LOG_FILE=/home/n800s/work/luchcity.git/scripts/runlib/luchcity.log

#список часов когда нужно синхронизировать время и расписание
SYNC_HOUR_LIST='0:00 11:19 16:00'

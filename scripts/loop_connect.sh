#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/data/init.inc
. $SCRIPTPATH/runlib/lib.sh

LOG_PREFIX='LOOP CONNECT'

lecho "Starting $0 at `date`"
$SCRIPTPATH/runlib/connect.sh
$SCRIPTPATH/runlib/sync_time.sh
$SCRIPTPATH/runlib/load_schedule.sh
$SCRIPTPATH/runlib/disconnect.sh
last_ds=`date +%d`
last_ts=`date +%H`
lecho "Starting neverending loop"
while true
do
	sleep $CONNECT_SLEEP
	ds=`date +%d`
	tsh=`date +%H`
	ms=`date +%M`
	for TS in $SYNC_HOUR_LIST
	do
		ts=`expr $tsh \* 60 + $ms`
		s_ts="$tsh:$ms"
		MS=`echo "$TS" | cut -d : -f 1`
		TS=`echo "$TS" | cut -d : -f 2`
		TS=`expr $MS \* 60 + $TS`
		lecho "Current time: $ds $s_ts ($ts)"
		if [ $ds -ne $last_ds ]
		then
			#next day, add 24 to $ts and $TS so they point to the next day time relatively to $last_ts
			lecho "$ds != $last_ds : Next day !!!"
			ts=`expr $ts + 24 \* 60`
			TS=`expr $TS + 24 \* 60`
		fi
		lecho "Test for $ts >= $TS and $last_ts < $TS"
		if ( [ $ts -ge $TS ] && [ $last_ts -lt $TS ] )
		then
			lecho "Synchronizing for day $ds time $s_ts"
			$SCRIPTPATH/runlib/connect.sh
			$SCRIPTPATH/runlib/sync_time.sh
			# reread current timestamp to avoid problem after time sync
			tsh=`date +%H`
			ms=`date +%M`
			last_ts=`expr $tsh \* 60 + $ms`
			last_ds=`date +%d`
			$SCRIPTPATH/runlib/load_schedule.sh
			$SCRIPTPATH/runlib/disconnect.sh
			lecho "Last used day and mins: $last_ds : $last_ts"
		fi
	done

done

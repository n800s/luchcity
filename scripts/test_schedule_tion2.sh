#!/bin/sh

start_date='2014-11-01 01:00'
increment=600

secs=`date -d "$start_date" -D '%Y-%m-%d %H:%M' '+%s'`
echo
while true
do
	ts=`date -d "$secs" -D '%s' '+%d.%m.%Y %H:%M'`
	printf "\r$ts"
	state=`runlib/s_onoff.sh "$ts"`
	secs=`expr $secs + $increment`
	if [ "x$state" != "x$old_state" ]
	then
		old_state=$state
		printf "\r$ts - $state\n"
	fi
done


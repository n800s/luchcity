#!/bin/sh

start_date='2015-01-30 01:00'
increment=600

secs=`date +%s --date "$start_date"`
echo
while true
do
	ts=`date --date @$secs '+%d.%m.%Y %H:%M'`
	printf "\r$ts"
	state=`runlib/s_onoff.sh "$ts"`
	secs=`expr $secs + $increment`
	if [ "x$state" != "x$old_state" ]
	then
		old_state=$state
		printf "\r$ts - $state\n"
	fi
done


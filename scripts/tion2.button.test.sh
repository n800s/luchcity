#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/data/init.inc


while true
do
	inp=`$SCRIPTPATH/bin/gpiorw -m r -p ${BUTTON_PORT} -b ${BUTTON_PIN}`
	if [ "x$inp" -eq "x1" ]
	then
		$SCRIPTPATH/bin/gpiorw -m w -p ${LIGHT_PORT} -b ${LIGHT_PIN} -v 1
		sleep 5
		$SCRIPTPATH/bin/gpiorw -m w -p ${LIGHT_PORT} -b ${LIGHT_PIN} -v 0
	fi
	sleep 1
done

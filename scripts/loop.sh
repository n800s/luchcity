#!/bin/sh

SCRIPTPATH=`dirname $0`
. $SCRIPTPATH/data/init.inc
. $SCRIPTPATH/runlib/lib.sh

lecho "Starting at `date`"
lecho "Sleeping for $START_PAUSE seconds"
sleep $START_PAUSE
while true
do
	$SCRIPTPATH/runlib/s_onoff.sh
	lecho "Sleeping for $LIGHTS_SLEEP seconds"
	sleep $LIGHTS_SLEEP

done

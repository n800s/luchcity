#!/usr/bin/env python

from datetime import timedelta, datetime
from runlib import lighter

starttime = datetime(year=2016, month=1, day=25)
endtime = datetime(year=2016, month=2, day=5)
timestep = timedelta(seconds=30)

# to stop time sync
lighter.MAX_TIME_OFFSET = 3600 * 24

l = lighter.Lighter()
l.debug = True
l.sync_if_needed()
laston = None
while starttime < endtime:
	val = l.on_off(starttime)
	starttime += timestep
	if l.laston != laston:
		if not l.laston:
			print '\t\t\t\t',
		print starttime.date(), starttime.time(), 'pin value:', val,
		print '^' if l.laston else 'v',
		if not l.laston:
			print
		laston = l.laston
print '\n\nEnd:', starttime

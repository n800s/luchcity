#!/usr/bin/env python

import sys, os, csv, subprocess, re, traceback, urllib2, errno, json
from syslog import syslog, openlog
from datetime import datetime, date, time, timedelta
from time import sleep
import ctypes
import ctypes.util

MAX_TIME_OFFSET = 2

class Lighter:

	def __init__(self, debug=False):
		self.debug = debug
		self.cfg = {}
		self.schedule = []
		self.next_sync_time = datetime.now().replace(second=0, microsecond=0)
		self.laston = None
		self.readconfig()
		self.form_schedule()

	def lecho(self, msg):
		msg = unicode(msg)
		if self.debug:
			print >>sys.stdout, msg
			sys.stdout.flush()
		if int(self.cfg.get('DEBUG_LOG', False)):
			syslog(msg)

	def calc_schedule_crc(self, schedule):
		rs = 0
		for line in schedule:
			self.lecho("Line '%s' has been read" % line)
			if line:
				self.lecho("Using '%s' for CRC calculation" % line)
				for dn in line[0].split('.'):
					rs += int(dn)
				for pr in line[1:]:
					for t in pr.split('-'):
						for tn in t.split(':'):
							if tn:
								rs += int(tn)
		return rs

	def is_on(self, curtime=None):
		rs = False
		if curtime is None:
			curtime = datetime.today()
		slen = len(self.schedule)
		for i in range(slen):
			if curtime.date() >= self.schedule[i]['date']:
				if (i+1 >= slen or curtime.date() < self.schedule[i+1]['date']):
	#				self.lecho('%s >= %s and < %s' % (curtime.date(), self.schedule[i]['date'], (self.schedule[i+1]['date'] if i< slen-1 else '')))
					for pr in self.schedule[i]['plist']:
						if ( pr[0] is None or curtime.time() >= pr[0]) and (pr[1] is None or curtime.time() <= pr[1]):
#							self.lecho('%s >= %s and < %s' % (curtime.time(), pr[0], pr[1]))
							rs = True
							break
				if rs:
					break
			else:
				break
		return rs

	def readconfig(self):
		cfg = {}
		ininame = os.path.join(os.path.dirname(__file__), '..', 'data', 'init.inc')
		reader = csv.reader(file(ininame), delimiter='=')
		for row in reader:
			if row:
				name = row[0].strip()
				if name and name[0] != '#':
					val = '='.join(row[1:]).strip()
					if len(val) > 2 and val[0] == '`' and val[-1] == '`':
						try:
							val = subprocess.check_output(val[1:-1], shell=True).strip()
						except Exception, e:
							self.lecho(e)
							val = ''
					val = re.sub(r'\$\{?\bSCRIPTPATH\b\}?', os.path.dirname(__file__), val)
					for k,v in cfg.items():
						val = re.sub(r'\$\{?\b' + k + r'\b\}?', v, val)
					cfg[name] = val
		self.cfg = cfg
		if int(self.cfg.get('DEBUG_LOG', False)):
			openlog("luchcity.py")
		self.lecho('%s' % self.cfg)

	def _form_schedule(self, schedule_text):
		lines = schedule_text.split('\n')
		lines = [line.split() for line in [line.strip() for line in lines] if line and line[0] != '#']
		if lines:
			if len(lines[0]) == 1:
				# CRC num
				crcnum = int(lines[0][0])
				lines = lines[1:]
			else:
				crcnum = None
			if not crcnum is None:
				calc_crc = self.calc_schedule_crc(lines)
				if crcnum != calc_crc:
					raise Exception('CRC error: %s != %s' % (crcnum, calc_crc))
			today = datetime.today()
			schedule = []
			for line in lines:
				plist = []
				schedule.append({'date': datetime.strptime(line[0], '%d.%m.%Y').date(), 'plist': plist})
				for pe in line[1:]:
					st,en = pe.split('-')
					if st:
						h1,m1 = [int(v) for v in st.split(':')]
						st = time(hour=h1, minute=m1)
					else:
						st = None
					if en:
						h2,m2 = [int(v) for v in en.split(':')]
						en = time(hour=h2, minute=m2)
					else:
						en = None
					if st and en:
						if st >= en:
							raise Exception('Error in the schedule: %s must not be bigger that %s' % (st.strftime('%H:%M'), en.strftime('%H:%M')))
					plist.append([st, en])
				rs = sorted(schedule, key=lambda v: v['date'])
		else:
			raise Exception('Bad schedule')
		return rs

	def form_schedule(self):
		if os.path.exists(self.cfg['SCHNAME']):
			fname = self.cfg['SCHNAME']
		else:
			fname = self.cfg['DEFAULT_SCHNAME']
		self.schedule = self._form_schedule(file(fname).read())

	def on_off(self, test_time=None):
		rs = None
		curtime = test_time if test_time else datetime.today()
		curon = self.is_on(curtime)
		if curon != self.laston:
			if curon:
				self.turn_on()
			else:
				self.turn_off()
			self.laston = curon
			if 'PI_GPIO' in self.cfg:
				rs = self.get_gpio(self.cfg['PI_GPIO'])
		return rs

	def use_gpio(self, gpio, val = None):
		rs = None
		exported = False
		if not os.path.exists('/sys/class/gpio/gpio%s' % gpio):
			file('/sys/class/gpio/export', 'w').write(str(gpio))
			exported = True
		try:
			# wait a little for the system to export the gpio
			sleep(3)
			if val is None:
				rs = int(file('/sys/class/gpio/gpio%s/value' % gpio, 'r').read())
			else:
				file('/sys/class/gpio/gpio%s/direction' % gpio, 'w').write('out')
				file('/sys/class/gpio/gpio%s/value' % gpio, 'w').write(str(val))
		finally:
			if exported:
				file('/sys/class/gpio/unexport', 'w').write(str(gpio))
		return rs

	def set_gpio(self, gpio, val):
		self.use_gpio(gpio, val)

	# test for the value
	def get_gpio(self, gpio):
		return self.use_gpio(gpio)

	def turn_on(self):
		if 'PI_GPIO' in self.cfg:
			if not self.debug:
				self.lecho('Turn ON at %s' % datetime.today().strftime('%c'))
			self.set_gpio(self.cfg['PI_GPIO'], 1)

	def turn_off(self):
		if 'PI_GPIO' in self.cfg:
			if not self.debug:
				self.lecho('Turn OFF at %s' % datetime.today().strftime('%c'))
			self.set_gpio(self.cfg['PI_GPIO'], 0)

	def run(self):
		while True:
			try:
				self.sync_if_needed()
				self.on_off()
				sleep(float(self.cfg['LIGHTS_SLEEP']))
			except:
				self.lecho(traceback.format_exc())
				sleep(10)

	def start_connection(self):
		if self.cfg.get('DIAL_SERVICE', None):
			rs = subprocess.Popen(self.cfg['DIAL_SERVICE'], shell=True)
			sleep(40);
			for i in range(5):
				status = subprocess.call(['ping', '-c', '1', self.cfg['PING_HOST']])
				if status == 0:
					break
				sleep(int(self.cfg['PING_WAIT']))
			if status:
				self.lecho("Can not ping %s" % self.cfg['PING_HOST'])
		else:
			rs = None
		return rs

	def stop_connection(self, connector):
		if self.cfg.get('DIAL_SERVICE_STOP', None):
			status = subprocess.call(self.cfg['DIAL_SERVICE_STOP'], shell=True)
			self.lecho('%s executed, exit status = %d' % (self.cfg['DIAL_SERVICE_STOP'], status))
		else:
			if not connector is None:
				connector.terminate()
				sleep(5)
				if connector.poll() is None:
					self.lecho('%s is still working' % self.cfg['DIAL_SERVICE'])
					connector.kill()
					sleep(5)
					if connector.poll() is None:
						self.lecho('%s is forever. it never dies!!!' % self.cfg['DIAL_SERVICE'])

	def sync_if_needed(self):
		#form list of times for today from SYNC_HOUR_LIST
		now = datetime.now().replace(second=0, microsecond=0)
		if now >= self.next_sync_time:
			self.lecho('Current time %s >= the next sync time %s, synchronizing ...' % (now, self.next_sync_time))
			p = self.start_connection()
			try:
				self.time_sync()
				self.load_schedule()
			finally:
				self.stop_connection(p)
			# find the next sync time
			done = False
			while not done:
				for hm in self.cfg['SYNC_HOUR_LIST'].split():
					h,m = (hm.split(':') + [0])[:2]
					t = now.replace(hour=int(h), minute=int(m))
					if t > self.next_sync_time:
						self.next_sync_time = t
						done = True
						break
				if not done:
					now += timedelta(days=1)

	def time_sync(self):
		wo = urllib2.urlopen('%s?%s' % (self.cfg['TIME_URL'], self.cfg['MAC']))
		timeline = wo.readline()
		seconds = int(timeline.split()[-1])
		dt = datetime.fromtimestamp(seconds)
		now = datetime.today()
		if abs((dt - now).total_seconds()) > MAX_TIME_OFFSET:
			self.lecho('Synchronizing time... old:%s set by new:%s' % (now, dt) )
			# sync
			# /usr/include/linux/time.h:
			#
			# define CLOCK_REALTIME                     0
			CLOCK_REALTIME = 0

			# /usr/include/time.h
			#
			# struct timespec
			#  {
			#    __time_t tv_sec;            /* Seconds.  */
			#    long int tv_nsec;           /* Nanoseconds.  */
			#  };
			class timespec(ctypes.Structure):
				_fields_ = [("tv_sec", ctypes.c_long),
					("tv_nsec", ctypes.c_long)]
			librt = ctypes.CDLL(ctypes.util.find_library("rt"), use_errno=True)
			ts = timespec()
			ts.tv_sec = seconds
			ts.tv_nsec = 0
			if librt.clock_settime(CLOCK_REALTIME, ctypes.byref(ts)) < 0:
				raise Exception('Error synchronizing time: %s' % os.strerror(ctypes.get_errno()))
			else:
				self.lecho('Time has been synchronized, now: %s' % datetime.now())

	def load_schedule(self):
		self.lecho("Removing old %s.tmp if it exists" % self.cfg['SCHNAME'])
		if os.path.exists(self.cfg['SCHNAME']):
			os.unlink(self.cfg['SCHNAME'])
		url = '%s?%s' % (self.cfg['SCHEDULER_URL'], self.cfg['MAC'])
		self.lecho('Loading schedule from %s' % url)
		wo = urllib2.urlopen(url)
		schedule_text = wo.read()
		self._form_schedule(schedule_text)
		file(self.cfg['SCHNAME'], 'w').write(schedule_text)

if __name__ == '__main__':

	from datetime import timedelta
	starttime = datetime(year=2014, month=1, day=1)
	endtime = datetime(year=2014, month=2, day=10)
	l = Lighter()
	l.sync_if_needed()
	l.turn_on()
	sleep(2)
	l.turn_off()
	laston = False
	while starttime < endtime:
		l.on_off(starttime)
		starttime += timedelta(seconds=600)
		if l.laston != laston:
			if not l.laston:
				print '\t\t\t\t',
			print starttime.date(), starttime.time(),
			print unichr(0x25b2 if l.laston else 0x25bc),
			if not l.laston:
				print
			laston = l.laston

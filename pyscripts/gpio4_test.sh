#!/bin/sh

if [ ! -f /sys/class/gpio/gpio4/value ]
then
    echo 4 > /sys/class/gpio/export
    EXPORTED=yes
    echo exported
    sleep 3
fi
echo Port value: `cat /sys/class/gpio/gpio4/value`
if [ "${EXPORTED}x" != "x" ]
then
    echo 4 > /sys/class/gpio/unexport
    echo unexported
fi
